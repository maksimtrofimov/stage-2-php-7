<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\Track;
use App\Task3\CarTrackHtmlPresenter;
use \App\Task1\Car;

$track = new Track(4, 40);
$bmw = new car(
    1,
    'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
    'BMW',
    250,
    10,
    5,
    15
);
$tesla = new car(
    2,
    'https://i.pinimg.com/originals/e4/15/83/e41583f55444b931f4ba2f0f8bce1970.jpg',
    'Tesla',
    200,
    5,
    5.3,
    18
);
$ford = new car(
    3,
    'https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg',
    'Ford',
    220,
    5,
    6.1,
    18.5
);
$track->add($bmw);
$track->add($tesla);
$track->add($ford);
$presenter = new CarTrackHtmlPresenter();
$presentation = $presenter->present($track);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
    <style>
        h1, h2 {
            text-align: center;
            color: rgba(0,0,0,0.6);
            text-shadow: 2px 8px 6px rgba(0,0,0,0.2),
            0px -5px 35px rgba(255,255,255,0.3);
        }

        body {
            background: url(https://img4.goodfon.ru/original/1366x768/0/80/overdraiv-overdrive-triller-poster-shosse-avtomobili-gonka-s.jpg) no-repeat center center fixed;
            background-size: cover;
            height: 100px;
            background-attachment: fixed ;
            background-position: top center;
        }
        .car {
            opacity: 0.1;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            text-align: center;
            margin: 1% 0;
            min-height: 200px;
            background-color: rgba(33, 33, 33, .7);
            align-items: center;transition: 1s;
        }
        .car:hover {
            opacity: 1;
            transition: 1s;
        }
        .car img {
            border-radius: 152px;
            margin: 0 auto;
        }
        .car img:first-of-type {
            border: 15px solid;
            -moz-border-image: url(https://besplatka.ua/aws/31/03/16/17/flag-150h90-sm-gonochnyi---finish---shahmatnyi--flagi-raznyh-photo-5690.jpg) 30 round round;
            -webkit-border-image: url(https://besplatka.ua/aws/31/03/16/17/flag-150h90-sm-gonochnyi---finish---shahmatnyi--flagi-raznyh-photo-5690.jpg) 30 round round;
            -o-border-image: url(https://besplatka.ua/aws/31/03/16/17/flag-150h90-sm-gonochnyi---finish---shahmatnyi--flagi-raznyh-photo-5690.jpg) 30 round round;
            border-image: url(https://besplatka.ua/aws/31/03/16/17/flag-150h90-sm-gonochnyi---finish---shahmatnyi--flagi-raznyh-photo-5690.jpg) 30 round round;
            border-radius: 0px;
        }
        .car span {
            font-weight: bold;
            font-size: 25px;
        }
    </style>
</head>
<body>
        <h1>CAR RACING</h1>
    <div class="main">
        <h2>LAST FIGHT</h2>
        <div class="car">
            <?php echo $presentation; ?>
        </div>
    </div>

</body>
</html>
