<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $winnerId = $track->run()->getId();
        $cars = $track->all();
        $string = '';
        foreach ($cars as $key => $car) {
            ++$key;
            $string .= "<img src=\"{$car->getImage()}\">" .
            "<h2>{$key}-</h2><span>{$car->getName()}: {$car->getSpeed()}, {$car->getFuelConsumption()}</span>";
        }
        return $string;
    }

}