<?php

declare(strict_types=1);

namespace App\Task1;

class Car
{
    private int $_id;
    private string $_image;
    private string $_name;
    private int $_speed;
    private int $_pitStopTime;
    private float $_fuelConsumption;
    private float $_fuelTankVolume;
    
    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) {
        $this->id = $id;
        $this->image = $image;
        $this->name = $name;
        $this->speed = $speed;
        $this->pitStopTime = $pitStopTime;
        $this->fuelConsumption = $fuelConsumption;
        $this->fuelTankVolume = $fuelTankVolume;
        $this->checkCarParam(func_get_args());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }

    public function checkCarParam($param)
    {
        foreach ($param as $key => $value) {
            if ($value < 0) {
                throw new \Exception("negative number of $key");
            }
        }
    }
}