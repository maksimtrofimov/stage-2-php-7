<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private float $_lapLength;
    private int $_lapsNumber;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
        $this->checkTrackParam(func_get_args());
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run() : Car
    {
        $distance = $this->distance();
        if (empty($this->cars)) {
            throw new \Exception("car not add");
        }
        foreach ($this->cars as $car) {
            $timeDistance = $distance / $car->speed * 60 * 60;
            $fuel = $distance / 100 * $car->fuelConsumption;
            $pitStopTime = ($fuel / $car->fuelTankVolume) * $car->pitStopTime;
            $places[] = $timeDistance + ($pitStopTime);
        }
        asort($places);
        $winner = array_key_first($places);
        return $this->cars[$winner];
    }

    private function distance() : float
    {
        return $this->lapLength * $this->lapsNumber;
    }

    public function checkTrackParam($param)
    {
        foreach ($param as $key => $value) {
            if ($value < 0) {
                throw new \Exception("negative number of $key");
            }
        }
    }

}