<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    private string $_title;
    private int $_price;
    private int $_pagesNumber;

    public function __construct(string $title, int $price, int $pagesNumber)
    {
        $this->title = $title;
        $this->price = $price;
        $this->pagesNumber = $pagesNumber;
        $this->checkBookParam(func_get_args());
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }

    public function checkBookParam($param)
    {
        foreach ($param as $key => $value) {
            if ($value < 0) {
                throw new \Exception("negative number of $key");
            }
        }
    }
}