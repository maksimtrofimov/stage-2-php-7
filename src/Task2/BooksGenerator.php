<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    public function __construct(
        int $minPagesNumber,
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
    ) {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks   = $libraryBooks;
        $this->maxPrice       = $maxPrice;
        $this->storeBooks     = $storeBooks;
    }

    public function generate(): \Generator
    {
        foreach ($this->libraryBooks as $book) {
            if ($book->pagesNumber >= $this->minPagesNumber) {
                yield $book;
            }
        }

        foreach ($this->storeBooks as $book) {
            if ($this->maxPrice > $book->price) {
                yield $book;
            }
        }
    }
}