<?php

require __DIR__ . '/vendor/autoload.php';

$car1 = new App\Task1\Car(
    3,
    'https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg',
    'Ford',
    230,
    5,
    4,
    25
);
$car2 = new App\Task1\Car(
    1,
    'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
    'BMW',
    250,
    40,
    35,
    10
);

    $track = new App\Task1\Track(10, 100);
    $track->add($car1);
    $track->add($car2);
    $track->run();
